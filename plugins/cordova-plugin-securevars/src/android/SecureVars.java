package cordova.plugin.securevars;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class echoes a string called from JavaScript.
 */
public class SecureVars extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("getSecureVar")) {
            this.getSecureVar(args, callbackContext);
            return true;
        }
        return false;
    }

    private void getSecureVar(JSONArray args, CallbackContext callbackContext) {
        if (args != null){
            try {
                int deseada = Integer.parseInt(args.getJSONObject(0).getString("variable"));
                String retorno = "";

                switch(deseada) {
                case 1:
                    retorno = "Variable deseada 1";
                    break;
                case 2:
                    retorno = "Variable deseada 2";
                    break;
                default:
                    retorno = "";
                }

                callbackContext.success(retorno);
            }
            catch (Exception exception){
                callbackContext.error("");
            }
        }
        else {
            callbackContext.error("");
        }
    }
}
