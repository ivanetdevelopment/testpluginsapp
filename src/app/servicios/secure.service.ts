import { Injectable } from '@angular/core';

// import { Plugin, Cordova, CordovaProperty, CordovaInstance, IonicNativePlugin } from '@ionic-native/core';
import { IonicNativePlugin, cordova } from '@ionic-native/core';

// @Plugin({
//   pluginName: "securevars",
//   plugin: "cordova-plugin-securevars",
//   pluginRef: "SecureVars",
//   repo: "https://gitlab.com/ivanetdevelopment/securevars.git",
//   platforms: ['Android', 'iOS']
// })

@Injectable({
  providedIn: 'root'
})
export class SecureService extends IonicNativePlugin {
  static pluginName = "securevars";
  static plugin = "cordova-plugin-securevars";
  static pluginRef = "SecureVars";
  static repo = "https://gitlab.com/ivanetdevelopment/securevars.git";
  static platforms = ['Android', 'iOS']

  // @Cordova()
  // getSecureVar(arg1: any): Promise<string> {
  //   return;
  // }
  
  getSecureVar(arg1: any) {
    return cordova(this, 'getSecureVar', {}, [arg1]);
  }

}