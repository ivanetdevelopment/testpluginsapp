import { Component } from '@angular/core';
import { SecureService } from '../servicios/secure.service';
import { Platform } from '@ionic/angular';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  resultado: string = "Sin inicializar";


  constructor(public secure: SecureService, private platform: Platform) {
    this.platform.ready().then(() => {
      this.resultado = "Inicializado";
      this.secure.getSecureVar({variable: 1}).then(res => {
        this.resultado = res;
      }).catch(error => {
        this.resultado = "Error: " + error;
      });
    });
  }
}
